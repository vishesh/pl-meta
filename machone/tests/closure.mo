
(def mul
  (lambda (a b)
    (lambda (c)
      (* a b c))))

(def a 10)
(def b 10)

(println ((mul 10 11) 2))

(def ff (mul 2 4))
(println ff)
(println (ff 3))
