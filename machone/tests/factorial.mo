
(def factorial
  (lambda (n)
   (if (zero? n)
     1
     (* n (factorial (sub1 n))))))

(println (factorial 5))
(println (factorial 10))
