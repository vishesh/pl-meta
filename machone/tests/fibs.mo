
(def fibs
  (lambda (n)
    (if (<= n 1)
      n
      (+ (fibs (- n 1)) (fibs (- n 2))))))


(println (fibs 0))
(println (fibs 1))
(println (fibs 2))
(println (fibs 3))
(println (fibs 4))
(println (fibs 5))
(println (fibs 6))
(println (fibs 7))
(println (fibs 8))
(println (fibs 9))
(println (fibs 10))
(println (fibs 11))
(println (fibs 20))
