
(def pos?
  (lambda (n)
    (if (< n 0)
      0
      1)))

(println (pos? 10))
(println (pos? -10))
